// Quick start
const config = require('./config.json');
const FtpSrv = require('ftp-srv');
const FileSystem = FtpSrv.FileSystem;
const fs = require('fs');
const ftpServer = new FtpSrv('ftp://' + config.ftp.ip + ":" + config.ftp.port, {
	pasv_range: config.ftp.pasv
});

class CustomFileSystem extends FileSystem {
	constructor() {
		super(...arguments)
	}


	write(fileName) {
		const filename = Date.now() + '.pdf'
		const localFile = config.ftp.storageDir + filename;
		const stream = fs.createWriteStream(localFile);


		stream.on('error', (err) => {
			console.log('Captured error')
			console.log(err)
		})

		stream.on('finish', () => {
			console.log('finished uploading file to ' + localFile)
		});

		return stream;
	}
}

ftpServer.on('login', (data, resolve, reject) => {
	if(data.username === config.ftp.username && data.password === config.ftp.password) {
		console.log('Connected ' + data.username);
		const conn = data.connection;

		resolve({fs: new CustomFileSystem(conn)});

		conn.on('STOR', (error, fileName) => {
			console.log('received file ' + fileName)
		});

		conn.on('client-error', error => {
			console.log('Error captured: ' + error)
		})


	}
	else reject();

});
ftpServer.on('error', ({connection, context, error}) => {
	console.log('client experienced error' + error);
});
ftpServer.listen(config.ftp.port)
	.then(() => {
		console.log('Listening on ' + config.ftp.port)
	});